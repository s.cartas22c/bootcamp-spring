package com.example.springbootejecicios456.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Laptop {

    private String os;
    private String processor;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idLaptop;
    private Boolean wirelessNetworkAdapter;
    private Long hardDriveCapacity;
    private Integer weight;


    public Laptop() {
    }

    public Laptop(String os, String processor, Long idLaptop, Boolean wirelessNetworkAdapter, Long hardDriveCapacity, Integer weight) {
        this.os = os;
        this.processor = processor;
        this.idLaptop = idLaptop;
        this.wirelessNetworkAdapter = wirelessNetworkAdapter;
        this.hardDriveCapacity = hardDriveCapacity;
        this.weight = weight;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public Long getIdLaptop() {
        return idLaptop;
    }

    public void setIdLaptop(Long idLaptop) {
        this.idLaptop = idLaptop;
    }

    public Boolean getWirelessNetworkAdapter() {
        return wirelessNetworkAdapter;
    }

    public void setWirelessNetworkAdapter(Boolean wirelessNetworkAdapter) {
        this.wirelessNetworkAdapter = wirelessNetworkAdapter;
    }

    public Long getHardDriveCapacity() {
        return hardDriveCapacity;
    }

    public void setHardDriveCapacity(Long hardDriveCapacity) {
        this.hardDriveCapacity = hardDriveCapacity;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
