package com.example.springbootejecicios456;

import com.example.springbootejecicios456.entities.Laptop;
import com.example.springbootejecicios456.repositories.LaptopRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringbootEjecicios456Application {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(SpringbootEjecicios456Application.class, args);
		LaptopRepository repository = (LaptopRepository) context.getBean("laptopRepository");

		Laptop laptop1 = new Laptop("Windows 11", "30000M", null, true, 80L, 15);
			repository.save(laptop1);

			Laptop laptop2 = new Laptop("LINUX", "xr012", null, true, 55L, 30);
				repository.save(laptop2);


	}



}
