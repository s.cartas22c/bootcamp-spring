package com.example.springbootejecicios456.controllers;

import com.example.springbootejecicios456.entities.Laptop;
import com.example.springbootejecicios456.repositories.LaptopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LaptopController {

    @Autowired
    LaptopRepository repository;

    @GetMapping("/api/laptops")
    public ResponseEntity<List<Laptop>> getAllLaptops(){
        return ResponseEntity.ok(repository.findAll());
    }


   @PostMapping("api/laptops")
    public ResponseEntity<Laptop> createLaptop(@RequestBody Laptop laptop){
        return ResponseEntity.ok(repository.save(laptop));
   }
}
