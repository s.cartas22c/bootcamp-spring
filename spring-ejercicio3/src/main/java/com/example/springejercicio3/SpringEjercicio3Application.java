package com.example.springejercicio3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringEjercicio3Application {

	public static void main(String[] args) {


		ApplicationContext context = SpringApplication.run(SpringEjercicio3Application.class, args);


		BookRepository myRepository = (BookRepository) context.getBean("bookRepository");

		System.out.println("Total books:" + myRepository.count());

		System.out.println("--- Insert new book ---");

		Book book1 = new Book();
		book1.setEditorial("Soul");
		book1.setTitle("The universe of minimalist");
		book1.setPublicationYear(2021);
		myRepository.save(book1);

		System.out.println("--- Insert new book ---");

		Book book2 = new Book();
		book2.setEditorial("FIX");
		book2.setTitle("Java, all in your pocket");
		book2.setPublicationYear(2000);
		myRepository.save(book2);

		Book book3 = new Book();
		book3.setEditorial("FIX");
		book3.setTitle("Python, all in your pocket");
		book3.setPublicationYear(2010);
		myRepository.save(book3);

		System.out.println("Total books:" + myRepository.count());

		myRepository.findAll().stream().forEach(p -> System.out.println(p.getIdBook() + " " + p.getTitle()));


	}

}
