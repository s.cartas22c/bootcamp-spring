package com.example.obrestdatajpa;

import com.example.obrestdatajpa.entities.Book;
import com.example.obrestdatajpa.repositories.BookRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class ObRestDatajpaApplication {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(ObRestDatajpaApplication.class, args);
		BookRepository bookRepository = (BookRepository) context.getBean("bookRepository");

		//CRUD

		// New book
		Book book1 = new Book(null,"Life of Pi","Yann Martel", 460,"Novel", true);
		Book book2 = new Book(null,"The Help","Kathryn Stockett", 464,"SM", false);

		//Insert
		bookRepository.save(book1);
		bookRepository.save(book2);

		//getAll books
		List<Book>  books = bookRepository.findAll();
		System.out.println("Total books:" + books.size());
 		books.stream().forEach(p -> System.out.println("Title:" + p.getTitle()));

 		//Delete book
		/*bookRepository.deleteById(2L);
		System.out.println("Delete book 2");
		List<Book>  booksAfter = bookRepository.findAll();
		System.out.println("Total books:" + booksAfter.size());*/


	}

}
