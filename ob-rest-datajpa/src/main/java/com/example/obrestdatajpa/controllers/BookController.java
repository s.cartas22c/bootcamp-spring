package com.example.obrestdatajpa.controllers;

import com.example.obrestdatajpa.entities.Book;
import com.example.obrestdatajpa.repositories.BookRepository;
import jdk.jshell.Snippet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class BookController {

    @Autowired
    BookRepository repository;

    @GetMapping("/api/books")
    public List<Book> findAllBooks(){
        return  repository.findAll();
    }

    @GetMapping("/api/books/{id}")
    public ResponseEntity<Book> findByID(@PathVariable Long id){
        Optional<Book>  book = repository.findById(id);
        return  book.isPresent() ?   ResponseEntity.ok(book.get()) : ResponseEntity.notFound().build();
    }

    @PostMapping("/api/books")
    public ResponseEntity<Book> create(@RequestBody Book book){
        repository.save(book);
        return ResponseEntity.ok(book);
    }

    @DeleteMapping("api/book/{id}")
    public HttpStatus delete(@PathVariable Long id){
        repository.deleteById(id);
        return HttpStatus.OK;
    }



}
