
## Spring Boot
Dependencias agregadas

* Spring Data JPA SQL
Persist data in SQL stores with Java Persistence API using Spring Data and Hibernate.


* H2 Database SQL
Provides a fast in-memory database that supports JDBC API and R2DBC access, with a small (2mb) footprint. Supports embedded and server modes as well as a browser based console application.


* Spring WEB
Build web, including RESTful, applications using Spring MVC. Uses Apache Tomcat as the default embedded container.


* Spring Boot DevTools DEVELOPER TOOLS
Provides fast application restarts, LiveReload, and configurations for enhanced development experience.


Aplicación API REST con acceso a base de datos H2 para persistir la información.


Clases generadas:

* Book - Entity
* BookRepository - Gestor CRUD persistencia de datos
* BookController - Gestión peticiones REST


Flujo de creación:

Creación de la entidad BOOK gestionado por anotaciones de Spring.

| Annotation      |            | Description |
|-----------------|:----------:|------------:|
| @Entity         | Spring JPA |             |
| @Table          | Spring JPA |             |
| @Id             | Spring JPA |             |
| @GeneratedValue | Spring JPA |             |

Creación de interfaz Repository con Spring JPA que permita realizar acciones CRUD sobre la entidad creada.


| Annotation  |            | Description |
|-------------|:----------:|------------:|
| @Repository | Spring JPA |             |


Creación controlador BookController

| Annotation       |                      | Description |
|------------------|:--------------------:|------------:|
| @RestController  | Spring Framework WEB |             |


Publicación de métodos para realizar acciones CRUD sobre la entidad Book.

| Action     |    Mapping     |                     URL |
|------------|:--------------:|------------------------:|
| findAll    |  @GetMapping   |              /api/books |
| findByID   |  @GetMapping   |            /api/book/id |
| createBook |  @PostMapping  |              /api/books |
| deleteBook | @DeleteMapping |            /api/book/id |


