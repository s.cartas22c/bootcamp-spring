package org.example;

public class GestorFacturas {

    CalculatorService caluladoraFacturas;
    String saludo;
    public GestorFacturas(CalculatorService caluladoraFacturas, String saludo) {
        System.out.println("Constructor GestorFacturas con argumento");
        this.caluladoraFacturas = caluladoraFacturas;
        System.out.println("Desde constructor:" + caluladoraFacturas.helloWorld());
        this.saludo = saludo;
    }

    public void helloGestorFacturas(){
        System.out.println(saludo);
    }
}
