package com.example.springdatajpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringDatajpaApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringDatajpaApplication.class, args);
		CarRepository myCarRepository = (CarRepository) context.getBean("carRepository");

		System.out.println("Total cars:" + myCarRepository.count());

	}

}
