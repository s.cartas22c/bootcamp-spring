package com.example.springdatajpa;

import javax.persistence.*;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long keyCode;
    private String manufacturer;
    private String model;
    private int buildYear;


    public Car() {
    }

    public Car(long keyCode, String manufacturer, String model, int buildYear) {
        this.keyCode = keyCode;
        this.manufacturer = manufacturer;
        this.model = model;
        this.buildYear = buildYear;
    }

    public long getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(long keyCode) {
        this.keyCode = keyCode;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getBuildYear() {
        return buildYear;
    }

    public void setBuildYear(int buildYear) {
        this.buildYear = buildYear;
    }
}
