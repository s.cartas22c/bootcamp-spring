package com.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        CalculatorService myCalculator = (CalculatorService) context.getBean("calculatorService");
        System.out.println(myCalculator.helloWorld());

        GestorFacturas myGestor = (GestorFacturas) context.getBean("gestorFacturas");
        System.out.println("La calculadora saluda desde el gestor ->" + myGestor.caluladoraFacturas.helloWorld());
        System.out.println("MyGestor saluda ->");
        myGestor.helloGestorFacturas();


    }
}